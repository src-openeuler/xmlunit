Name:           xmlunit
Version:        2.10.0
Release:        1
Summary:        Unit Testing XML for Java and .NET
License:        Apache 2.0 and BSD
URL:            https://www.xmlunit.org/
Source0:        https://github.com/xmlunit/xmlunit/releases/download/v%{version}/%{name}-%{version}.tar.gz
Patch1:         0001-Port-to-hamcrest-2.1.patch
BuildArch:      noarch

BuildRequires:  maven-local
BuildRequires:  mvn(com.sun.istack:istack-commons-runtime)
BuildRequires:  mvn(com.sun.xml.bind:jaxb-impl)
BuildRequires:  mvn(jakarta.activation:jakarta.activation-api)
BuildRequires:  mvn(javax.xml.bind:jaxb-api)
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(net.bytebuddy:byte-buddy)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-shade-plugin)
BuildRequires:  mvn(org.assertj:assertj-core)
BuildRequires:  mvn(org.hamcrest:hamcrest-core)
BuildRequires:  mvn(org.hamcrest:hamcrest-library)
BuildRequires:  mvn(org.mockito:mockito-core)
Requires:       junit, xalan-j2, xml-commons-apis
Provides:       %{name}-javadoc%{?_isa} %{name}-javadoc
Obsoletes:      %{name}-javadoc

%description
XMLUnit provides you with the tools to verify the XML you emit is the one you want to create.
It provides helpers to validate against an XML Schema, assert the values of XPath queries or
compare XML documents against expected outcomes.

%package        assertj
Summary:        Assertj for %{name}
%description    assertj
This package provides %{summary}.

%package        core
Summary:        Core package for %{name}
%description    core
This package provides %{summary}.

%package        legacy
Summary:        Legacy package for %{name}
%description    legacy
This package provides %{summary}.

%package        matchers
Summary:        Matchers for %{name}
%description    matchers
This package provides %{summary}.

%package        placeholders
Summary:        Placeholders for %{name}
%description    placeholders
This package provides %{summary}.

%prep
%autosetup -n %{name}-%{version} -p1

%pom_disable_module xmlunit-assertj3
%pom_disable_module xmlunit-jakarta-jaxb-impl
%pom_remove_plugin org.codehaus.mojo:buildnumber-maven-plugin
%pom_remove_plugin :maven-assembly-plugin
%pom_remove_plugin org.cyclonedx:cyclonedx-maven-plugin

%pom_add_dep com.sun.istack:istack-commons-runtime::test  xmlunit-core
sed -i 's/private boolean isElementOfCustomAssert/protected boolean isElementOfCustomAssert/g' xmlunit-assertj/src/main/java/org/xmlunit/assertj/CustomAbstractAssert.java
%mvn_alias "org.xmlunit:xmlunit-legacy" "xmlunit:xmlunit"

%build
%mvn_build -s -f

%install
%mvn_install


%files -f .mfiles-xmlunit-parent
%doc README.md CONTRIBUTING.md RELEASE_NOTES.md
%license LICENSE
%{_javadocdir}/%{name}/*

%files assertj -f .mfiles-xmlunit-assertj
%files core -f .mfiles-xmlunit-core
%files legacy -f .mfiles-xmlunit-legacy
%files matchers -f .mfiles-xmlunit-matchers
%files placeholders -f .mfiles-xmlunit-placeholders

%changelog
* Wed Jan 08 2025 Ge Wang <wang__ge@126.com> - 2.10.0-1
- Update to version 2.10.0

* Wed Jun 19 2024 Ge Wang <wang__ge@126.com> - 2.9.1-2
- Fix build error for assertj-core-3.23.1

* Mon Mar 11 2024 Ge Wang <wang__ge@126.com> - 2.9.1-1
- Update to version 2.9.1

* Wed Sep 27 2023 wangkai <13474090681@163.com> - 2.7.0-2
- Fix build error for hamcrest-2.x

* Mon Feb 21 2022 Ge Wang <wangge20@huawei.com> - 2.7.0-1
- Upgrade to version 2.7.0

* Tue Dec 3 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.6-9
- Package init
